
width = 900
height = 900
-- attractors = { {width/2, 0},{width, height}, {0, height} }
attractors = {}
for i=1,4
do
  table.insert( attractors, {math.random( width ), math.random( height )} )
end

points_cords = {{width/3,height/2}}
new_point = {{width/3,height/2}, {255,255,255}}
div = 2

function table.slice(tbl, first, last, step)
  local sliced = {}

  for i = first or 1, last or #tbl, step or 1 do
    sliced[#sliced+1] = tbl[i]
  end

  return sliced
end

function love.load()
   local f = love.graphics.newFont(12)
   love.graphics.setFont(f)
   love.graphics.setColor(255,255,255)
   love.graphics.setBackgroundColor(0,0,0)
   love.window.setMode( width, height, {vsync=0,msaa=0,depth=1,borderless=true} )
end

function love.update(dt)
--  if love.keyboard.isDown("c")
--  then

--  end

--  if love.keyboard.isDown("r")
--  then

--  end

  if love.keyboard.isDown("q")
  then
    love.event.quit()
  end

--  if love.keyboard.isDown("f")
--  then

--  end


  random_attractor_index = math.random( #attractors )
  att_cords = attractors[ random_attractor_index]
--  last_x = points_cords[#points_cords][1]
--  last_y = points_cords[#points_cords][2]
  last_x = new_point[1][1]
  last_y = new_point[1][2]
  att_x = att_cords[1]
  att_y = att_cords[2]
  
  new_point = { ((last_x-att_x)/div)+att_x, ((last_y-att_y)/div)+att_y }
  new_point = { new_point, {(new_point[1])%1, (new_point[2])%1, (new_point[1]+new_point[2])%1} }
  new_point[2][random_attractor_index] = 1
--  table.insert( points_cords, new_point )
  -- for index, value in pairs(new_point) do print(value) end
  -- print(#points_cords)
end

function love.draw()
      -- love.timer.sleep(0.0001)
      love.graphics.setColor(new_point[2])
      -- love.graphics.print(points_cords[#points_cords][1]..' '..points_cords[#points_cords][2], 0, 0)
      love.graphics.points(new_point[1])
--      points_cords = table.slice(points_cords, #points_cords-1, #points_cords)
end

love.graphics.clear( )
love.graphics.clear = function() end
